﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotateCube : MonoBehaviour {

    public GameObject cubee;
    private bool cuboGira;

    public GameObject capsula;

	// Use this for initialization
	void Start () {

        cuboGira = false;

    }
	
	// Update is called once per frame
	void Update () {

        capsula.transform.Rotate(Vector3.right * 100 * Time.deltaTime);

        if (cuboGira)
            cubee.transform.Rotate(Vector3.right *100* Time.deltaTime);

    }

    public void giraCubo()
    {

        cuboGira = true;

    }
}
