﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class googleAPI : MonoBehaviour
{

    private RawImage img;

    string urlMap;

    string urlPath;

    public string localOrig;
   //public float lonOrig;

    //public string localDest;
    public string localDest;

    LocationInfo li;

    public int zoom;
    public int mapWidth;
    public int mapHeight;

    public enum mapType { roadmap, satellite, hybrid, terrain }
    public mapType mapSelected;
    public int scale;

    private float[] turnPointsLat = new float[100];
    private float[] turnPointsLong = new float[100];
    public Text errorText;


    IEnumerator Map()
    {
        string wholePathJSON;

        urlPath = "https://maps.googleapis.com/maps/api/directions/json?origin=" + localOrig + "&destination=" + localDest + "&key=AIzaSyDLCPwQAFruyIwVq8iK4gO6M4JbxkIYv7c";

        WWW www2 = new WWW(urlPath);
        yield return www2;

        if (www2.text == "")
        {
            errorText.text = "Got connection problems";
            yield break;
        }

        else
            errorText.text = "I got 99 problems but my connection ain't one";


        wholePathJSON = www2.text;

        string patternEnd = @"end_location(\n|.)*?([-+]?[0-9]*\.[0-9]+|[0-9]+)(\n|.)*?([-+]?[0-9]*\.[0-9]+|[0-9]+)(\n|.)*?}";      //regex to get all end_locations
        Regex regexEndLocations = new Regex(patternEnd, RegexOptions.IgnoreCase);                                        //need to ignore the first (which is a duplicate of the final endlocation) and 
                                                                                                                            //get groups 2 and 4 as the latitude and longitude, respectively
        Match matchesEndLoc = regexEndLocations.Match(wholePathJSON);
        int matchCount = 0;
        while (matchesEndLoc.Success)
        {
            /* Debug.Log(" Group 2 (lat): " + matchesEndLoc.Groups[2].Value);
            Debug.Log(" Group 4 (long): " + matchesEndLoc.Groups[4].Value);*/

            turnPointsLat[matchCount] = float.Parse(matchesEndLoc.Groups[2].Value);
            turnPointsLong[matchCount] = float.Parse(matchesEndLoc.Groups[4].Value);

            matchesEndLoc = matchesEndLoc.NextMatch();
            matchCount++;
        }


        string patternStart = @"start_location(\n|.)*?([-+]?[0-9]*\.[0-9]+|[0-9]+)(\n|.)*?([-+]?[0-9]*\.[0-9]+|[0-9]+)(\n|.)*?}";      //regex to get all startLocations 
        Regex regexStartLocations = new Regex(patternStart, RegexOptions.IgnoreCase);

        Match matchStartLoc = regexStartLocations.Match(wholePathJSON);

        if (matchStartLoc.Success)                                                     //getting only the first startLocation with an if instead of a while
        {
            turnPointsLat[0] = float.Parse(matchStartLoc.Groups[2].Value);         //substitutes the first endlocation with the startLocation
            turnPointsLong[0] = float.Parse(matchStartLoc.Groups[4].Value);
        }


        string markers = null;

        int i = 0;

        while (i < matchCount)
        {
            markers = markers + "&markers=color:blue%7Clabel:" + i.ToString() + "%7C" + turnPointsLat[i].ToString() + "," + turnPointsLong[i].ToString();      //encodes all locations as markers
            i++;
        }

        if (markers == null)
        {
            // Debug.Log("Não há caminho disponível");
            errorText.text = "Não há caminho disponível\nentre os pontos " + localOrig + " e " + localDest;
        }

        while (true)
        {

            urlMap = "https://maps.googleapis.com/maps/api/staticmap?center=" + (turnPointsLat[0]+turnPointsLat[matchCount-1])/2f + "," + (turnPointsLong[0] + turnPointsLong[matchCount - 1]) / 2f +       //centers the map in the geographic middle point between origin and destination
                "&zoom=" + zoom + "&size=" + mapWidth + "x" + mapHeight + "&scale=" + scale
                + "&maptype=" + mapSelected + markers +                 //inserts the encoded markers into the display map
                "&key=AIzaSyDLCPwQAFruyIwVq8iK4gO6M4JbxkIYv7c";
            WWW www = new WWW(urlMap);
            yield return www;
            img.texture = www.texture;
            img.SetNativeSize();

        }
        
    }
    // Use this for initialization
    void Start()
    {
        img = gameObject.GetComponent<RawImage>();
        StartCoroutine(Map());
        //Map();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}