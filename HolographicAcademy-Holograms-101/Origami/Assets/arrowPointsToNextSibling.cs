﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arrowPointsToNextSibling : MonoBehaviour {

    private Transform target;
    private int index;
    public GameObject mainCamera;

	// Use this for initialization
	void Start () {

        index = transform.GetSiblingIndex();
       // Debug.Log("criado filho #" + index.ToString());

       // Debug.Log("temos " + transform.parent.childCount.ToString() + " filhos");

    }
	
	// Update is called once per frame
	void Update () {

//se for muito pesado, podemos tirar isso do update. botar apenas no start, porém fazendo o objeto atual mandar o objeto anterior (index - 1, que já foi criado) apontar para o atual

        if (transform.parent.childCount > index + 1)
        {

            target = transform.parent.GetChild(index + 1).transform;

            //Vector3 targetPos = new Vector3(transform.localPosition.x, transform.localPosition.y, target.transform.localPosition.z);

            transform.LookAt(target.transform.localPosition);

            if (transform.GetSiblingIndex() == 1)
            {
                transform.localPosition = new Vector3(transform.localPosition.x, mainCamera.transform.localPosition.y, 2f);
            }

        }
        else
            transform.localEulerAngles = new Vector3(90f, 0f, 0f);

        transform.localPosition = new Vector3(transform.localPosition.x, mainCamera.transform.localPosition.y - 1f, transform.localPosition.z);

    }
}
