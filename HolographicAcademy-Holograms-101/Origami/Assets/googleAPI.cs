﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class googleAPI : MonoBehaviour
{

    private RawImage img;

    string urlMap;

    string urlPath;

    public string localOrig;
   //public float lonOrig;

    //public string localDest;
    public string localDest;

    public GameObject threeDWaypointObjBase;


    LocationInfo li;

    public int zoom;
    public int mapWidth;
    public int mapHeight;

    public enum mapType { roadmap, satellite, hybrid, terrain }
    public mapType mapSelected;
    public int scale;

    private float[] turnPointsLat = new float[100];
    private float[] turnPointsLong = new float[100];
    //public Text errorText;
    public Text errorText2;

   
    IEnumerator Map()
    {
        string wholePathJSON;

        urlPath = "https://maps.googleapis.com/maps/api/directions/json?origin=" + localOrig + "&destination=" + localDest + "&key=AIzaSyDLCPwQAFruyIwVq8iK4gO6M4JbxkIYv7c";

        WWW www2 = new WWW(urlPath);
        yield return www2;


        if (www2.text == "")
        {
            errorText2.text = www2.error;
          //  errorText.text = "Got connection problems";
            yield break;
        }

      //  else
         //   errorText.text = "I got 99 problems but my connection ain't one";


        wholePathJSON = www2.text;

        string patternEnd = @"end_location(\n|.)*?([-+]?[0-9]*\.[0-9]+|[0-9]+)(\n|.)*?([-+]?[0-9]*\.[0-9]+|[0-9]+)(\n|.)*?}";      //regex to get all end_locations
        Regex regexEndLocations = new Regex(patternEnd, RegexOptions.IgnoreCase);                                        //need to ignore the first (which is a duplicate of the final endlocation) and 
                                                                                                                            //get groups 2 and 4 as the latitude and longitude, respectively
        Match matchesEndLoc = regexEndLocations.Match(wholePathJSON);
        int matchCount = 0;
        while (matchesEndLoc.Success)
        {
            /* Debug.Log(" Group 2 (lat): " + matchesEndLoc.Groups[2].Value);
            Debug.Log(" Group 4 (long): " + matchesEndLoc.Groups[4].Value);*/

            turnPointsLat[matchCount] = float.Parse(matchesEndLoc.Groups[2].Value);
            turnPointsLong[matchCount] = float.Parse(matchesEndLoc.Groups[4].Value);

            matchesEndLoc = matchesEndLoc.NextMatch();
            matchCount++;
        }


        string patternStart = @"start_location(\n|.)*?([-+]?[0-9]*\.[0-9]+|[0-9]+)(\n|.)*?([-+]?[0-9]*\.[0-9]+|[0-9]+)(\n|.)*?}";      //regex to get all startLocations 
        Regex regexStartLocations = new Regex(patternStart, RegexOptions.IgnoreCase);

        Match matchStartLoc = regexStartLocations.Match(wholePathJSON);

        if (matchStartLoc.Success)                                                     //getting only the first startLocation with an if instead of a while
        {
            turnPointsLat[0] = float.Parse(matchStartLoc.Groups[2].Value);         //substitutes the first endlocation with the startLocation
            turnPointsLong[0] = float.Parse(matchStartLoc.Groups[4].Value);
        }


        string markers = null;

        int i = 0;

        while (i < matchCount)
        {
            markers = markers + "&markers=color:blue%7Clabel:" + i.ToString() + "%7C" + turnPointsLat[i].ToString() + "," + turnPointsLong[i].ToString();      //encodes all locations as markers
            i++;
        }

        if (markers == null)
        {
            // Debug.Log("Não há caminho disponível");
            errorText2.text = "Não há caminho disponível\nentre os pontos " + localOrig + " e " + localDest;
        }

        threeDWaypointObjBase.transform.GetChild(0).gameObject.SetActive(true);


        Transform spawnPos;
        GameObject obj2Bcloned;
       // GameObject clone1;

        spawnPos = threeDWaypointObjBase.transform.GetChild(0).transform;
        obj2Bcloned = threeDWaypointObjBase.transform.GetChild(0).gameObject;

        //  clone1 = Instantiate(obj2Bcloned, spawnPos.position, spawnPos.rotation);
        //Instantiate(obj2Bcloned, spawnPos.position, spawnPos.rotation);
        // Instantiate(obj2Bcloned, spawnPos.position, spawnPos.rotation);

        //  clone1.transform.localPosition = new Vector3(0.1f, 0.1f, 2f);
        

        i = 0;

      //  float origLatMeters = latitudeToMetersOrig(turnPointsLat[0]);
       // float origLonMeters = longitudeToMetersOrig(turnPointsLong[0], turnPointsLat[0]);

        while (i < matchCount)
        {
            GameObject clone1;
            clone1 = Instantiate(obj2Bcloned, spawnPos.position, spawnPos.rotation);

            clone1.transform.parent = threeDWaypointObjBase.transform.parent;

            //Debug.Log("long = " + turnPointsLong[i].ToString());

            clone1.transform.localPosition = new Vector3(longitudeToMeters(turnPointsLong[i], turnPointsLat[i], turnPointsLong[0], turnPointsLat[0]), 0f, latitudeToMeters(turnPointsLat[i], turnPointsLat[0]));
           // turnPointsLat[i].ToString() + "," + turnPointsLong[i].ToString();      //encodes all locations as markers
            i++;
        }

        threeDWaypointObjBase.transform.GetChild(0).gameObject.SetActive(false);


        while (true)
        {

            urlMap = "https://maps.googleapis.com/maps/api/staticmap?center=" + (turnPointsLat[0]+turnPointsLat[matchCount-1])/2f + "," + (turnPointsLong[0] + turnPointsLong[matchCount - 1]) / 2f +       //centers the map in the geographic middle point between origin and destination
                "&zoom=" + zoom + "&size=" + mapWidth + "x" + mapHeight + "&scale=" + scale
                + "&maptype=" + mapSelected + markers +                 //inserts the encoded markers into the display map
                "&key=AIzaSyDLCPwQAFruyIwVq8iK4gO6M4JbxkIYv7c";
            WWW www = new WWW(urlMap);
            yield return www;
            img.texture = www.texture;
            img.SetNativeSize();

        }
        
    }
    // Use this for initialization
    void Start()
    {
        img = gameObject.GetComponent<RawImage>();
        StartCoroutine(Map());
        //Map();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

   /* float latitudeToMetersOrig(float lat)
    {
        float meters = lat * 111111;
        return meters;
    }

    float longitudeToMetersOrig(float lon, float lat)
    {
        float meters = 0;
        // 111111 * cos(lat) meters = 1 degree lon
        meters = lon * 111111 * (float)(Math.Cos(lat));

        Debug.Log("long original em  metros = " + meters.ToString());

        return meters;
    }*/

    float latitudeToMeters(float lat, float origLat)
    {
        lat = lat - origLat;
        float meters = lat * 111111;
        return meters;
    }

    float longitudeToMeters(float lon, float lat, float origLon, float origLat)
    {
        float meters = 0;
        // 111111 * cos(lat) meters = 1 degree lon
        lon = lon - origLon;

        double Pi = 3.1415;

        meters = lon * 111111 * (float)(Math.Cos((Pi * lat / 180)));

        return meters;
    }
}