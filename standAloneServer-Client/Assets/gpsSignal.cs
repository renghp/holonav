﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class gpsSignal : MonoBehaviour
{
    private bool gpsIsOn = false;
    public Text gpsText;
    private string gpsOutput;


    void Start()
    {
        gpsOutput = "Waiting for GPS signal";
        Input.location.Start();
        StartCoroutine(getGPS());
    }

    IEnumerator getGPS()
    {
        Debug.Log("1");
        // First, check if user has location service enabled
        if (!Input.location.isEnabledByUser)
            yield break;

        // Start service before querying location


        // Wait until service initializes
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            gpsOutput = "Timed out";
            Debug.Log("Timed out");
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            gpsOutput = "Unable to determine device location";
            Debug.Log("Unable to determine device location");
            yield break;
        }
        else
        {
            gpsIsOn = true;
            gpsOutput = "Lat: " + Input.location.lastData.latitude + " Lon: " + Input.location.lastData.longitude + " Alt: " + Input.location.lastData.altitude + " horizontal acc: " + Input.location.lastData.horizontalAccuracy + "TS: " + Input.location.lastData.timestamp;


            Debug.Log("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
        }

        // Stop service if there is no need to query location updates continuously
        //Input.location.Stop();
    }

    void Update()
    {
        gpsText.text = gpsOutput;

        if (gpsIsOn)
        {
            gpsIsOn = false;
            StartCoroutine(getGPS());

        }

    }
}

/*using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class gpsSignal : MonoBehaviour
{
	public bool gpsIsOn = false;
	public Text gpsText;
	
	
	 void Start()
    {
		Input.location.Start();
		 StartCoroutine(getGPS());
	}
	
    IEnumerator getGPS()
    {
		Debug.Log("1");
        // First, check if user has location service enabled
        if (!Input.location.isEnabledByUser)
            yield break;

        // Start service before querying location
        

        // Wait until service initializes
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
			gpsText.text = "Timed out";
            Debug.Log("Timed out");
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
			gpsText.text ="Unable to determine device location";
            Debug.Log("Unable to determine device location");
            yield break;
        }
        else
        {
            gpsIsOn = true;

            while (gpsIsOn)
            { 
                //we can change the boolean to false from another function or here in an if statement
			    gpsText.text = "Lat: " + Input.location.lastData.latitude + " Lon: " + Input.location.lastData.longitude + " Alt: " + Input.location.lastData.altitude + " horizontal acc: " + Input.location.lastData.horizontalAccuracy + "TS: " + Input.location.lastData.timestamp;
			    
			    //Debug.Log("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);

            }

        }

        // Stop service if there is no need to query location updates continuously
        Input.location.Stop();
    }
	
	void Update()
	{
		

            
	}
}*/
