﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

public class myIP : MonoBehaviour {
	
	public Text feedback;
	public Text feedback2;

	// Use this for initialization
	void Start () {
	
	

	
	}
	
	
	// Update is called once per frame
	void Update () {
		
		feedback.text = "Local WIFI IP: \n" + GetLocalIPv4(NetworkInterfaceType.Wireless80211); //for wifi
		feedback2.text = "Local Ethernet IP: \n" + GetLocalIPv4(NetworkInterfaceType.Ethernet);	//for ethernet cable
		
	}
	

	public string GetLocalIPv4(NetworkInterfaceType _type)
	{
		string output = "";
		foreach (NetworkInterface item in NetworkInterface.GetAllNetworkInterfaces())
		{
			if (item.NetworkInterfaceType == _type && item.OperationalStatus == OperationalStatus.Up)
			{
				foreach (UnicastIPAddressInformation ip in item.GetIPProperties().UnicastAddresses)
				{
					if (ip.Address.AddressFamily == AddressFamily.InterNetwork)
					{
						output = ip.Address.ToString();
					}
				}
			}
		}
    return output;
	}
}